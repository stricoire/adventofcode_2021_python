
import statistics


def calculate_fuel_cost(positionToAlignOn, crabPosition):
	fuelCost = 0
	fuelCost=sum(map(lambda x : abs(positionToAlignOn - x), crabPosition))
	
	return fuelCost


# part 1 : 21 min
def exo7_part1():
	print("#### EXO 7 part 1 #####")
	f = open('input_7.txt', 'r')
		
	lines = f.readline()
	crabPosition = lines.split(',')
	crabPosition = list(map(int, crabPosition))
	
	
	positionToAlignOn = statistics.median(crabPosition)
	print("positionToAlignOn (Median) is :", positionToAlignOn)
	
	fuelCost = calculate_fuel_cost(positionToAlignOn, crabPosition)
	print("FuelCost is ", fuelCost)

	f.close()



def help(x):
	total = 0
	for i in range (1,x+1):
		total+=i
	return total

def calculate_fuel_cost2(positionToAlignOn, crabPosition):
	fuelCost = 0
	fuelCost=sum(map(lambda x : help(abs(positionToAlignOn - x)), crabPosition))
	
	return fuelCost


# part 2 : 39 min
def exo7_part2():
	print("#### EXO 7 part2 #####")
	f = open('input_7.txt', 'r')
		
	lines = f.readline()
	crabPosition = lines.split(',')
	crabPosition = list(map(int, crabPosition))
	
	
	positionToAlignOn = round(statistics.mean(crabPosition))
	print("positionToAlignOn (mean) is :", positionToAlignOn)
	
	data2 = [1, 1, 10]
	data1 = [16, 1, 2, 0, 4, 2, 7, 1,2,14]
 
	x = round(statistics.mean(data1))

	
	fuelCost1 = calculate_fuel_cost2(positionToAlignOn, crabPosition)
	print("FuelCost is ", fuelCost1)
	
	fuelCost2 = calculate_fuel_cost2(positionToAlignOn-1, crabPosition)
	print("FuelCost is ", fuelCost2)
	
	fuelCost3 = calculate_fuel_cost2(positionToAlignOn+1, crabPosition)
	print("FuelCost is ", fuelCost3)
	
	print("Min FuelCost is ", min(fuelCost1, fuelCost2, fuelCost3))
	
	f.close()


exo7_part1()
exo7_part2()
