# AdventOfCode 2021 challenges in Python

In this repository you'll find my source code (<i>in Python</i>) for all the challenges I succeeded so far.

- It was my first time doing it, it was quite fun, and allowed me to practice again algorithm using Python.

- I don't know if I'll finish them all, I saw it more as a way of practicing my code, after I had released my game (<i>feel free to test it out [here](https://lumbirbwut.itch.io/grandma-and-the-flowers)</i>), a way for me to practice on small projects before winter holidays.

| Days | Time for challenge 1 | Time for challenge 2        |
|------|----------------------|-----------------------------|
|   1  | 1 hour               | 30 minutes                  |
|   2  | 19 minutes           | 5 minutes                   |
|   3  | 30 minutes           | 3 hours                     |
|   4  | 1 hour, 50 minutes   | 10 minutes                  |
|   5  | 1 hour, 10 minutes   | 31 minutes                  |
|   6  | 46 minutes           | 1 minute                    |
|   7  | 21 minutes           | 39 minutes                  |
|   8  | 25 minutes           | 3 hours, 19 minutes         |
|   9  | 36 minutes           | 1 hour, 27 minutes          |
|  10  | 1 hour, 07 minutes   | 24 minutes                  |
|  11  | 1 hour, 17 minutes   | 24 minutes                  |
|  12  | 1 hour, 13 minutes   | 5 minutes                   |
|  13  | 2 hours, 27 minutes  | 2 minutes                   |
|  14  | 30 minutes           | 2 hours                     |
|  15  | 5 hours, 30 minutes  | 6 hours (still not working) |
