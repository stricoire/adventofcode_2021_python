from queue import PriorityQueue


class Graph:
	def __init__(self, heightGraph, widthGraph, repetition):
		self.height = heightGraph
		self.width = widthGraph
		self.nodesCount = heightGraph * widthGraph
		self.nodes = [-1 for j in range (self.nodesCount)] 
		self.repetitionCount = repetition
		self.visited = []
		
		
	def add_risk(self, node, weightNode):
		self.nodes[node] = weightNode
		

	def print_nodes(self):
		result = ""
		for i in range (len(self.nodes)):
			result+=str(self.nodes[i])
			
		print(result)
	
	def get_line_of_node(self,node):
		return int(node / (self.width*self.repetitionCount))
		
		
	
	def get_column_of_node(self,node):
		return node % (self.width*self.repetitionCount)
		
		
		

'''
def dijkstra(graph, startNode, endNode):
	pathLengths = {v: float('inf') for v in range (graph.nodesCount)}
	pathLengths[startNode] = 0
	
	pq = PriorityQueue()
	pq.put((0, startNode))
	
	while not pq.empty():
		# We get the nearest vertex (aka:smallestCostPath)
		(pathCost, currentNode) = pq.get()
		graph.visited.append(currentNode)
		
		if(currentNode == endNode):
			break
		
		print(currentNode,pathCost)
		
		lineNode = graph.get_line_of_node(currentNode)
		columnNode = graph.get_column_of_node(currentNode)	
		#print("la :" ,currentNode,lineNode,columnNode)
		
		
		# if not last line, you got a down neighbor
		if (lineNode < graph.height - 1):
			#print("down")
			neighborNode = currentNode + graph.width
			if(neighborNode not in graph.visited):
				distanceNeighbor = graph.nodes[neighborNode]
				newPathCost = pathCost + distanceNeighbor
				# If this path is shorter than previous
				if(newPathCost < pathLengths[neighborNode]):
					priority = newPathCost + heuristic(neighborNode, endNode,graph)
					pq.put((priority, neighborNode))
					pathLengths[neighborNode] = newPathCost
		
		# if not first line, you got a up neighbor
		if (lineNode > 0):
			#print("up")
			#print(currentNode)
			neighborNode = currentNode - graph.width
			if(neighborNode not in graph.visited):
				distanceNeighbor = graph.nodes[neighborNode]
				newPathCost = pathCost + distanceNeighbor
				# If this path is shorter than previous
				if(newPathCost < pathLengths[neighborNode]):
					priority = newPathCost + heuristic(neighborNode, endNode,graph)
					pq.put((priority, neighborNode))
					pathLengths[neighborNode] = newPathCost
			
		# if not last column, you got a right neighbor
		if (columnNode < graph.width - 1):
			#print("right")
			neighborNode = currentNode + 1
			if(neighborNode not in graph.visited):
				distanceNeighbor = graph.nodes[neighborNode]
				newPathCost = pathCost + distanceNeighbor
				# If this path is shorter than previous
				if(newPathCost < pathLengths[neighborNode]):
					priority = newPathCost + heuristic(neighborNode, endNode,graph)
					pq.put((priority, neighborNode))
					pathLengths[neighborNode] = newPathCost
		
		# if not first column, you got a left neighbor
		if (columnNode > 0):
			#print("left")
			neighborNode = currentNode - 1
			if(neighborNode not in graph.visited):
				distanceNeighbor = graph.nodes[neighborNode]
				newPathCost = pathCost + distanceNeighbor
				# If this path is shorter than previous
				if(newPathCost < pathLengths[neighborNode]):
					priority = newPathCost + heuristic(neighborNode, endNode,graph)
					pq.put((priority, neighborNode))
					pathLengths[neighborNode] = newPathCost

	return pathLengths
'''

def A_star(graph, startNode, endNode, repetition):
	pathLengths = {v: float('inf') for v in range (graph.nodesCount * repetition * repetition)}
	pathLengths[startNode] = 0
	
	pq = PriorityQueue()
	pq.put((0, startNode))
	
	while not pq.empty():
		# We get the nearest vertex (aka:smallestCostPath)
		(priority, currentNode) = pq.get()
		graph.visited.append(currentNode)
		
		if(currentNode == endNode):
			break
		
		#print(currentNode,priority)
		
		lineNode = graph.get_line_of_node(currentNode) 
		columnNode = graph.get_column_of_node(currentNode) 
		#print("la :" ,currentNode,lineNode,columnNode)
		
		
		# if not last line, you got a down neighbor
		if (lineNode < (graph.height * repetition) - 1):
			#print("down")
			neighborNode = currentNode + graph.width * repetition
			#print(neighborNode, currentNode)
			if(neighborNode not in graph.visited):
				distanceNeighbor = get_new_risk(graph, neighborNode, repetition)
				newPathCost = pathLengths[currentNode] + distanceNeighbor
				# If this path is shorter than previous
				if(newPathCost < pathLengths[neighborNode]):
					priority = newPathCost + heuristic(neighborNode, endNode,graph)
					pq.put((priority, neighborNode))
					pathLengths[neighborNode] = newPathCost
		
		# if not first line, you got a up neighbor
		if (lineNode > 0):
			#print("up")
			#print(currentNode)
			neighborNode = currentNode - graph.width * repetition
			if(neighborNode not in graph.visited):
				distanceNeighbor = get_new_risk(graph, neighborNode, repetition)
				newPathCost = pathLengths[currentNode] + distanceNeighbor
				# If this path is shorter than previous
				if(newPathCost < pathLengths[neighborNode]):
					priority = newPathCost + heuristic(neighborNode, endNode,graph)
					pq.put((priority, neighborNode))
					pathLengths[neighborNode] = newPathCost
			
		# if not last column, you got a right neighbor
		if (columnNode < (graph.width * repetition) - 1):
			#print("right")
			neighborNode = currentNode + 1
			if(neighborNode not in graph.visited):
				distanceNeighbor = get_new_risk(graph, neighborNode, repetition)
				newPathCost = pathLengths[currentNode] + distanceNeighbor
				# If this path is shorter than previous
				if(newPathCost < pathLengths[neighborNode]):
					priority = newPathCost + heuristic(neighborNode, endNode,graph)
					pq.put((priority, neighborNode))
					pathLengths[neighborNode] = newPathCost
		
		# if not first column, you got a left neighbor
		if (columnNode > 0):
			#print("left")
			neighborNode = currentNode - 1
			if(neighborNode not in graph.visited):
				distanceNeighbor = get_new_risk(graph, neighborNode, repetition)
				newPathCost = pathLengths[currentNode] + distanceNeighbor
				# If this path is shorter than previous
				if(newPathCost < pathLengths[neighborNode]):
					priority = newPathCost + heuristic(neighborNode, endNode,graph)
					pq.put((priority, neighborNode))
					pathLengths[neighborNode] = newPathCost

	return pathLengths
      

def heuristic(node1, node2, graph):
	node1X = graph.get_column_of_node(node1)
	#print("column heuristic",node1X)
	node2X = graph.get_column_of_node(node2)
	node1Y = graph.get_line_of_node(node1)
	node2Y = graph.get_line_of_node(node2)
	# Manhattan distance on a square grid
	return abs(node1X - node2X) + abs(node1Y - node2Y)


def validity_function():
	repetition = 1
	g = Graph(3,4,repetition)
	g.add_risk(0, 0)
	g.add_risk(1, 1)
	g.add_risk(2, 2)
	g.add_risk(3, 3)
	g.add_risk(4, 4)
	g.add_risk(5, 5)
	g.add_risk(6, 6)
	g.add_risk(7, 7)
	g.add_risk(8, 8)
	g.add_risk(9, 9)
	g.add_risk(10, 0)
	g.add_risk(11, 0)
	
	
	assert(g.get_line_of_node(0)==0)
	assert(g.get_line_of_node(3)==0)
	assert(g.get_line_of_node(5)==1)
	assert(g.get_line_of_node(8)==2)
	assert(g.get_line_of_node(10)==2)
	assert(g.get_column_of_node(0)==0)
	assert(g.get_column_of_node(5)==1)
	assert(g.get_column_of_node(8)==0)
	assert(g.get_column_of_node(6)==2)
	assert(g.get_column_of_node(3)==3)

# repetitionCount == 1 => Same graph
def get_new_risk(graph, indexRepeted, repetitionCount):
	columnRepeted = indexRepeted % (graph.width*repetitionCount)
	column = indexRepeted % graph.width
	
	lineRepeted = int(indexRepeted / (graph.width*repetitionCount))
	line = lineRepeted % graph.height
	
	
	indexNode = line * graph.width + column
	risk = graph.nodes[indexNode]
	
	verticalIncrement = int(lineRepeted / graph.height)  
	horizontalIncrement = int(columnRepeted / graph.width)
	
	#print("Index ", indexRepeted ,"Column ",column,"line ", line)
	#print("Index ", indexRepeted ,"ColumnRepeted ",columnRepeted,"lineRepeted ", lineRepeted)
	#print("verticalIncrement ", verticalIncrement, "horizontalIncrement ", horizontalIncrement)
	
	newRisk = (risk + verticalIncrement + horizontalIncrement)%10
	if (newRisk == 0):
		newRisk += 1
		
	#print("Risk ", risk, "NewRisk ", newRisk)
	
	return newRisk

# part 1 : 3h50
# part 2 : 7h30
def exo15():
	
	print("#### EXO 15 #####")
	f = open('input_15.txt', 'r')
	
	lines = f.readlines()
	lineCount = len(lines)
	nodeCountPerLine = len(lines[0]) - 1

	repetition = 1
	
	g = Graph(lineCount,nodeCountPerLine,repetition)
	actualNode = 0
	# Part I : Parsing : Graph creation 
	for line in range(len(lines)):
		for column in range(len(lines[0])-1):
			weightNode = int(lines[line][column])
			g.add_risk(actualNode, weightNode)
			actualNode += 1
	
	
	#g.print_nodes()
	validity_function()
	
	endNode = g.nodesCount - 1	
	endNode = ((endNode + 1) * repetition * repetition) - 1
	
	print("EndNode ", endNode, " heuristic ",heuristic(0,endNode,g))
	
	
	D = A_star(g, 0,endNode,repetition)
	
	print(D)
	
	
	print("Distance from vertex 0 to vertex", endNode, "is", D[endNode])
	
	f.close()


exo15()


