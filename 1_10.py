




def print_data_points(dataPoints):
	for line in dataPoints:
		print(line)	


def is_chunk_closed(chunk):
	return chunk in [')',']','}','>']

def is_same_type_chunk(chunk1, chunk2):
	if ((chunk1 in  [')','(']) and (chunk2 in  [')','('])):
		return True
	
	if ((chunk1 in  [']','[']) and (chunk2 in   [']','['])):
		return True
	
	if ((chunk1 in  ['{','}']) and (chunk2 in  ['{','}'])):
		return True
	
	if ((chunk1 in  ['<','>']) and (chunk2 in  ['<','>'])):
		return True
	
	return False


def calculate_syntax_score(illegalsCharacter):
	score = 0
	for char in illegalsCharacter:
		if(char == ')'):
			score += 3
		
		elif(char == ']'):
			score += 57
		
		elif(char == '}'):
			score += 1197
		
		elif(char == '>'):
			score += 25137
	
	return score
			


def calculate_score_incomplete_line(incompleteLine):
	score = 0
	incompleteLine.reverse()
	for char in incompleteLine:
		score *= 5
		if(char == '('):
			score += 1
		
		elif(char == '['):
			score += 2
		
		elif(char == '{'):
			score += 3
		
		elif(char == '<'):
			score += 4
	
	return score


def validity_function():
	assert(is_chunk_closed(')'))
	assert(is_chunk_closed('(')) == False
	assert(is_chunk_closed(']'))
	assert(is_chunk_closed('>'))
	assert(is_chunk_closed('o')) == False
	assert(is_same_type_chunk('(', ')'))
	assert(is_same_type_chunk(')', '('))
	assert(is_same_type_chunk(']', '['))
	assert(is_same_type_chunk('{', '}'))
	assert(is_same_type_chunk('<', '>'))
	assert(is_same_type_chunk(']', '>')) == False
	assert(calculate_syntax_score([')',')',']','}','>'])) == 26397
	assert(calculate_score_incomplete_line(['<','{','(','['])) == 294
	assert(calculate_score_incomplete_line(['[','(','{','(','[','[','{','{'])) == 288957
	assert(calculate_score_incomplete_line(['(','{','[','<','{','('])) == 5566

# part 1 : 1h07
# part 2 : 24 min
def exo9_part1():
	print("#### EXO 9 part1 #####")
	f = open('input_10.txt', 'r')
		
	lines = f.readlines()
	
	validity_function()
		
	illegalsCharacter = []
	scoreListIncompleteLine = []
	
	for line in lines:
		lineList = []
		line = line.rstrip("\n")
		lineList[:0] = line
		#print(lineList)
	
		chunkList = []
	
		#print(lineList)	
		i = 0
		
		while True :
			chunk = lineList[i]
			if(i < (len(lineList)-1)):
				nextChunk = lineList[i+1]
				
				if(is_chunk_closed(nextChunk) and is_same_type_chunk(chunk, nextChunk)):
					# Here is i+2 because it goes to i+2-1 = i+1
					del(lineList[i:i+2])
					i -= 1
				
				# Corrupted line case
				elif (is_chunk_closed(nextChunk) and (not is_same_type_chunk(chunk, nextChunk))):
					illegalsCharacter.append(nextChunk)
					break #We go to next line
				
				# Open next chunk case
				else :
					i+=1
			
			#print(i, lineList)
			
			if(not lineList) :
				break
			
			if (i >= len(lineList)-1):
				print("IncompleteLine ",lineList)
				scoreListIncompleteLine.append(calculate_score_incomplete_line(lineList))
				break
		
	
		#print(lineList)	
		#print("illegalChar ", illegalsCharacter)
		

	
	print("illegalChar ", illegalsCharacter)
	score = calculate_syntax_score(illegalsCharacter)
	print("Score ", score)
	
	#print("ScoreListIncompleteLine ", scoreListIncompleteLine)
	scoreListIncompleteLine.sort()
	#print("ScoreListIncompleteLine ", scoreListIncompleteLine)
	index = int((len(scoreListIncompleteLine)-1)/2) 
	print("FinalScore : ",scoreListIncompleteLine[index])


	f.close()


exo9_part1()


