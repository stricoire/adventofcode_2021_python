# Python program for reading
# from file
 
 

def exo1_part1():
	print("#### EXO 1 part 1 #####")
	f = open('input_1.txt', 'r')
	increaseCount = 0
	line1 = f.readline()
	line1 = int(line1)
	lineCount = 1


	while True:
		line2 = f.readline()
		if not line2: break  # EOF
		
		line2 = int(line2)
		lineCount += 1
		if (line2 > line1):
				increaseCount += 1 
		line1 = line2
			

	f.close()
	
	print("Increase count : ", increaseCount)
	print("Line count : ", lineCount)



def exo1_part2():
	print("#### EXO 1 part 2 #####")
	f = open('input_1.txt', 'r')
	increaseCount = 0
	line1 = f.readline()
	line2 = f.readline()
	line3 = f.readline()
	lineCount = 3

	threeMeasurementSum1 = int(line1) + int(line2) + int(line3)
	threeMeasurementSum2 = 0

	while True:
		line1 = line2
		line2 = line3
		
		line3 = f.readline()
		if not line3: break  # EOF
		
		lineCount += 1
		threeMeasurementSum2 = int(line1) + int(line2) + int(line3)
		
		if (threeMeasurementSum2 > threeMeasurementSum1):
				increaseCount += 1 
		
		threeMeasurementSum1 = threeMeasurementSum2


		

	f.close()

	print("Increase count : ", increaseCount)
	print("Line count : ", lineCount)

exo1_part1()
#TEST_exo1_part1()
exo1_part2()

