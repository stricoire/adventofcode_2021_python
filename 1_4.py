# Python program for reading
# from file
 
 



def add_line_to_board(currentBoard, line, lineIndex):
	columnIndex = 0
	for number in line.split():
		currentBoard[lineIndex][columnIndex] = int(number)
		columnIndex +=1
	
	return currentBoard


def is_board_in_win_state(currentBoard, lineCurrentNumber, columnCurrentNumber):
	
	winState = True
	for j in range (5):
		if (currentBoard[lineCurrentNumber][j] != -1):
			 winState = False
			 break
	if(winState):
		#print("Etat de Win ", winState)
		return True
	
	winState = True
	for i in range (5):
		if (currentBoard[i][columnCurrentNumber] != -1):
			winState = False
			break
	
	#print("Etat de Win ", winState)
	return winState
	

def calculate_final_score(currentBoard, winningNumber):
	finalScore = 0
	for i in range (5):
		for j in range(5): 
			if (currentBoard[i][j] != -1):
				finalScore += currentBoard[i][j]
	
	#print("Final Sum is :", finalScore)
	finalScore *= winningNumber
	#print("Final Score is :", finalScore)
	return finalScore
	

def line_to_numbers(line):
	numbers = list(map(int,line.split(",")))
	return numbers


# return : isNumberInBoard, isWinState, Score
def is_number_in_board(currentBoard, number):
	
	isNumberInBoard = False
	lineCurrentNumber = -1
	columnCurrentNumber = -1
	
	for i in range (5):
		if (isNumberInBoard):
			break
		for j in range (5):
			if (currentBoard[i][j] ==  number):
				currentBoard[i][j] = -1 
				lineCurrentNumber = i
				columnCurrentNumber = j
				isNumberInBoard = True
				break
	
	if (not isNumberInBoard):
		return False, False, -1
	
	#print(currentBoard)
	isWinState = is_board_in_win_state(currentBoard, lineCurrentNumber, columnCurrentNumber)
	
	
	finalScore = -1
	if (isWinState):
		finalScore = calculate_final_score(currentBoard, number)
		
	
	return True, isWinState, finalScore


# return : IsBingo, Score, turnCount
def is_it_a_bingo(currentBoard, numberList):
	isWinState = False
	Score = -1
	turnCount = 0
	for number in numberList:
		turnCount += 1
		isNumberInBoard, isWinState, Score = is_number_in_board(currentBoard, number)
		if (isWinState):
			break
	
	if(isWinState):
		return True, Score, turnCount
	else:
		return False, -1, turnCount



# 1h50 pour la part 1; <10 min pour la part2
def exo4_part1():
	print("#### EXO 4 part 1 #####")
	f = open('input_4.txt', 'r')
	
	bingoNumbers = f.readline()
	print(bingoNumbers)
	numbers = line_to_numbers(bingoNumbers)
	
	#lines = f.readlines()
	
	board = [[0]*5 for i in range(5)]


	finalScore = 0
	minimalTurn = 10000 
	finalScoreLooser = 0 
	maximalTurn = 0
	while True:
		emptyLine = f.readline()
		if not emptyLine: break  # EOF
		
		line0 = f.readline()
		board = add_line_to_board(board, line0, 0)
		
		line1 = f.readline()
		board = add_line_to_board(board, line1, 1)
		
		line2 = f.readline()
		board = add_line_to_board(board, line2, 2)
		
		line3 = f.readline()
		board = add_line_to_board(board, line3, 3)
		
		line4 = f.readline()
		board = add_line_to_board(board, line4, 4)
		
		IsBingo, currentScore, turnCount = is_it_a_bingo(board, numbers)
		print(IsBingo, currentScore, turnCount)
		
		if (IsBingo):
			if(turnCount < minimalTurn):
				finalScore = currentScore
				minimalTurn = turnCount 
				
			if(turnCount > maximalTurn):
				finalScoreLooser = currentScore
				maximalTurn = turnCount 

		

	
	print("\n")
	print("FinalScore is : " , finalScore, "minimalTurn is : ", minimalTurn)
	print("finalScoreLooser is : " , finalScoreLooser, "maximalTurn is : ", maximalTurn)

	f.close()


exo4_part1()






