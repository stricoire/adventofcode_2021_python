




def print_data_points(dataPoints):
	for line in dataPoints:
		print(line)	


def is_low_point(pLine, pColumn, dataPoints):
	lastLine = len(dataPoints) - 1
	lastColumn = len(dataPoints[0]) - 1
		
	if (dataPoints[pLine][pColumn] == 9):
		return False, -1
	
	pValue = dataPoints[pLine][pColumn]
	
	# Has left neighbor
	if (pColumn > 0):
		if (pValue > dataPoints[pLine][pColumn-1]):
			return False, -1
	
	# Has right neighbor
	if (pColumn < lastColumn):
		if (pValue > dataPoints[pLine][pColumn+1]):
			return False, -1
	
	# Has up neighbor
	if (pLine > 0):
		if (pValue > dataPoints[pLine-1][pColumn]):
			return False, -1
	
	# Has down neighbor
	if (pLine < lastLine):
		if (pValue > dataPoints[pLine+1][pColumn]):
			return False, -1
	
	return True, pValue
	

def sum_risk_low_points(lowPoints):
	result = sum(lowPoints) + len(lowPoints)
	return result
	


# part 1 : 36 min
def exo9_part1():
	print("#### EXO 9 part1 #####")
	f = open('input_9.txt', 'r')
		
	lines = f.readlines()
	
	dataPoints=[]
	
	# Parsing values
	for line in lines:
		line = line.rstrip("\n")
		points = []
		for point in line:
			points.append(int(point))
		dataPoints.append(points)
	
	lowPoints = []
	
	
	for line in range (len(dataPoints)):
		for column in range (len(dataPoints[0])):
			bLowPoint, pValue = is_low_point(line, column, dataPoints)
			if(bLowPoint):
				lowPoints.append(pValue)
		
			
	
	print("Low Points are : ", lowPoints)
	result = sum_risk_low_points(lowPoints)
	print(result)
	
	f.close()



def is_part_of_bassin(pLine, pColumn, dataPoints):
	lastLine = len(dataPoints) - 1
	lastColumn = len(dataPoints[0]) - 1
	
	if (dataPoints[pLine][pColumn] == 9):
		return False
	
	# Has left neighbor in bassin
	if (pColumn > 0):
		if(dataPoints[pLine][pColumn-1] != 9):
			return True
	
	# Has up neighbor
	if (pLine > 0):
		if(dataPoints[pLine-1][pColumn] != 9):
			return True
	return False
		

def determine_bassin(pLine, pColumn, dataPoints, bassin, typePoint):
	lastLine = len(dataPoints) - 1
	lastColumn = len(dataPoints[0]) - 1
	
	if (dataPoints[pLine][pColumn] == 9 or dataPoints[pLine][pColumn] ==-1):
		return bassin
	
	pValue = dataPoints[pLine][pColumn]
	
	bassin.append(pValue)
	dataPoints[pLine][pColumn] = -1
	
	# Has left neighbor
	if ((pColumn > 0) and (pColumn <=pColumn) and (typePoint != "rightNeighbor")):
		determine_bassin(pLine, pColumn-1, dataPoints, bassin, "leftNeighbor")
	
	
	# Has right neighbor
	if ((pColumn < lastColumn) and (pColumn >=0) and (typePoint != "leftNeighbor")):
		determine_bassin(pLine, pColumn+1, dataPoints, bassin, "rightNeighbor")
	
	
	# Has up neighbor
	if ((pLine > 0) and (pLine <= lastLine) and (typePoint != "downNeighbor")):
		determine_bassin(pLine-1, pColumn, dataPoints, bassin, "upNeighbor")
	
	
	# Has down neighbor
	if ((pLine < lastLine) and (pLine >= 0) and (typePoint != "upNeighbor")):
		determine_bassin(pLine+1, pColumn, dataPoints, bassin, "downNeighbor")
	

	return bassin




def get_final_value_with_bassins(bassinList):
	bassinList.sort(key=lambda x: -len(x))
	return len(bassinList[0]) *  len(bassinList[1]) * len(bassinList[2]) 

# part 2 : 1h + 27 min
def exo9_part2():
	print("#### EXO 9 part2 #####")
	f = open('input_9.txt', 'r')
		
	lines = f.readlines()
	
	dataPoints=[]
	
	# Parsing values
	for line in lines:
		line = line.rstrip("\n")
		points = []
		for point in line:
			points.append(int(point))
		dataPoints.append(points)
	
	bassinList = []
	
	
	for line in range (len(dataPoints)):
		for column in range (len(dataPoints[0])):			
			if (not dataPoints[line][column]==9):
				if((not dataPoints[line][column]==-1) and (not is_part_of_bassin(line, column, dataPoints))):
					bassin = []
					bassinList.append(determine_bassin(line, column, dataPoints, bassin,"None"))
			
			
	
	#print("bassins are : ", bassinList)
	print("finalResult is: ", get_final_value_with_bassins(bassinList))
	
	f.close()





exo9_part1()
exo9_part2()

