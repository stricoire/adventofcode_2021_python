




def print_data_points(dataPoints):
	for line in dataPoints:
		print(line)	

def find_all_paths(graph, start, end, b_SmallCaveVisitedTwice, path=[]):
	path = path + [start]
	if start == end:
		return [path]
	if start not in graph:
		return []
	paths = []
	for node in graph[start]:
		if node not in path:
			newpaths = find_all_paths(graph, node, end, b_SmallCaveVisitedTwice,path)
			for newpath in newpaths:
				paths.append(newpath)
		elif ((node in graph) and node.isupper()):
			newpaths = find_all_paths(graph, node, end, b_SmallCaveVisitedTwice,path)
			for newpath in newpaths:
				paths.append(newpath)
		# Comment to get the answer of part 1
		elif ((node in graph) and node.islower() and not b_SmallCaveVisitedTwice):
			newpaths = find_all_paths(graph, node, end, True,path)
			for newpath in newpaths:
				paths.append(newpath)
		
	return paths       




# part 1 : 1h13
# part 2 : 5 min
def exo12():
	global gVar_FlashCount
	
	print("#### EXO 12 #####")
	f = open('input_12.txt', 'r')
		
	lines = f.readlines()
	
	graph = {}
	
	for line in lines:
		line = line.rstrip("\n")
		line = line.split('-')
		if(line[1] != 'start'):
			if(line[0] not in graph):
				graph[line[0]] = list({line[1]:1})
			else:
				graph[line[0]].extend({line[1]:1})
			
		if (line[1]!='end' and line[0]!='start'):
			if(line[1] not in graph):
				graph[line[1]] = list({line[0]:1})
			else:
				graph[line[1]].extend({line[0]:1})

	#print("gRAPH ", graph)
	
	path = find_all_paths(graph, 'start', 'end', False,path=[])
	#print_data_points(path)
	
	print(len(path))
	
	f.close()


exo12()


