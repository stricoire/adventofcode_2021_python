# Python program for reading
# from file
 
 

	
def get_bit_one_count(linesOfFile, codeSize):
	bitOneCount = [0]*codeSize
	for line in linesOfFile:
		index = 0
		for bit in line :
			if (bit == "1"):
				bitOneCount[index] += 1
			
			index +=1
	
	return bitOneCount

def get_gama_and_espilon(bitOneCount, numbersCount):
	gamaRate = ""
	espilonRate = ""
	for count in bitOneCount:
		if (count >= numbersCount/2):
			gamaRate +=str(1)
			espilonRate +=str(0)
		else :
			gamaRate+= str(0)
			espilonRate+= str(1)
	
	return gamaRate, espilonRate


# 30min et 38 sec
def exo3_part1():
	print("#### EXO 3 part 1 #####")
	f = open('input_3.txt', 'r')
	
	lines = f.readlines()
	
	numbersCount = len(lines)
	codeSize = len(lines[0]) - 1
	bitOneCount = get_bit_one_count(lines, codeSize)
	
	gamaRate, espilonRate = get_gama_and_espilon(bitOneCount, numbersCount)

	
	print("codeSize ", codeSize)
	print("numbersCount ", numbersCount, " ", numbersCount/2)
	print("bitOneCount ", bitOneCount)
	print("gamaRate ", gamaRate)
	print("epsilonRate ", espilonRate)
	
	powerConsumption = int(gamaRate, 2) * int(espilonRate, 2)
	print("Power Consumption", powerConsumption)

	
	f.close()



def actualise_bit_one_count(element, bitOneCount, index):
	for i in range (index, len(bitOneCount)):
		if (element[i] == "1"):
			bitOneCount[i] -= 1
	print("Ici: " ,bitOneCount)		
	return bitOneCount


def get_oxygen_generator_rating(data, bitOneCount, mostCommonBinaryValue, indexToCheck):
	#print("\n data ", data)
	#print("bitOneCount ", bitOneCount)
	#print("mostCommonBinaryValue ", mostCommonBinaryValue)
	#print("indexToCheck ", indexToCheck)
	
	
	remainingValues = []
	
	for index in range(len(data)):
		element = data[index]
		
		if (mostCommonBinaryValue == int(element[indexToCheck])):
			remainingValues.append(element)
			
		
		if(indexToCheck + 1 <= len(element)-1):
			if (mostCommonBinaryValue != int(element[indexToCheck])):
				bitOneCount = actualise_bit_one_count(element, bitOneCount, indexToCheck)
				
	
	#print("\n Remaining Values ", remainingValues, " size/2 ", len(remainingValues)/2)
	#print("bitOneCount ", bitOneCount)
	
	if (len(remainingValues) == 1):
		print("\n Remaining Values ", remainingValues)
		return remainingValues[0]
		
	
	if (bitOneCount[indexToCheck+1] >= len(remainingValues)/2):
		mostCommonBinaryValue = 1
	else :
		mostCommonBinaryValue = 0
		
	
	return get_oxygen_generator_rating(remainingValues, bitOneCount, mostCommonBinaryValue, indexToCheck+1)
	

def get_C02_scrubber_rating(data, bitOneCount, leastCommonBinaryValue, indexToCheck):
	print("\n data ", data)
	print("bitOneCount ", bitOneCount)
	

	remainingValues = []
	
	for index in range(len(data)):
		element = data[index]
		
		if (leastCommonBinaryValue == int(element[indexToCheck])):
			remainingValues.append(element)
			
		
		if(indexToCheck + 1 <= len(element)-1):
			if (leastCommonBinaryValue != int(element[indexToCheck])):
				bitOneCount = actualise_bit_one_count(element, bitOneCount, indexToCheck)
			
	
	#print("\n Remaining Values ", remainingValues, " size/2 ", len(remainingValues)/2)
	#print("bitOneCount ", bitOneCount)
	
	if (len(remainingValues) == 1):
		print("\n Remaining Values ", remainingValues)
		return remainingValues[0]
		
	
	
	if (bitOneCount[indexToCheck+1] < len(remainingValues)/2):
		leastCommonBinaryValue = 1
	else :
		leastCommonBinaryValue = 0
		
	
	return get_C02_scrubber_rating(remainingValues, bitOneCount, leastCommonBinaryValue, indexToCheck+1)
	

		
def get_bit_one_count_and_data(linesOfFile, codeSize, data):
	bitOneCount = [0]*codeSize
	for line in linesOfFile:
		data.append(line.rstrip("\n"))
		index = 0
		for bit in line :
			if (bit == "1"):
				bitOneCount[index] += 1
			
			index +=1
	
	return bitOneCount, data		


#2h56
def exo3_part2():
	print("\n#### EXO 2 part 2 #####")
	f = open('input_3.txt', 'r')
	
	lines = f.readlines()
	
	numbersCount = len(lines)
	codeSize = len(lines[0]) - 1
	data = []
	bitOneCount, data = get_bit_one_count_and_data(lines, codeSize, data)
	bitOneCountCopy = bitOneCount.copy()
	
	gamaRate, espilonRate = get_gama_and_espilon(bitOneCount, numbersCount)
	
	print("codeSize ", codeSize)
	print("numbersCount ", numbersCount, " ", numbersCount/2)
	print("bitOneCount ", bitOneCount)
	print("gamaRate ", gamaRate)
	print("epsilonRate ", espilonRate)

	
	powerConsumption = int(gamaRate, 2) * int(espilonRate, 2)
	print("Power Consumption", powerConsumption)
	
	print("\n === get_oxygen_generator_rating ")
	oxigenRating = get_oxygen_generator_rating(data, bitOneCount, int(gamaRate[0]), 0)
	oxigenRating = int(oxigenRating, 2)
	print("oxigenRating: ", oxigenRating)
	
	print("\n === get_C02_scrubber_rating ")
	co2Rating = get_C02_scrubber_rating(data, bitOneCountCopy, int(espilonRate[0]), 0)
	co2Rating = int(co2Rating,2)
	print("co2Rating: ", co2Rating)
	
	
	lifeSupport = oxigenRating * co2Rating
	print("LifeSupport: ", lifeSupport)
	
	f.close()
	

#exo3_part1()
exo3_part2()





