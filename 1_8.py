

def is_pattern_contained(patternSmall, patternLong):
	for i in patternSmall:
		if (i not in patternLong):
			return False
	
	
	return True
	
def is_pattern_equal(pattern1, pattern2):
	return len(pattern1) == len(pattern2) and sorted(pattern1) == sorted(pattern2)

#1,4,7,8
def facility_easy_patterns(patterns, digitToPattern):
	for values in patterns:
		#Stand for 1
		if(len(values)==2):
			digitToPattern[1] = values
			
		#Stand for 4
		elif(len(values)==4):
			digitToPattern[4] = values
			
		#Stand for 7
		elif(len(values)==3):
			digitToPattern[7] = values
			
		#Stand for 8
		elif(len(values)==7):
			digitToPattern[8] = values


#0,2,3,5,6,9
def facility_harder_patterns(patterns, digitToPattern):
	for values in patterns:		
		#Stand for 0 or 6 or 9		
		if(len(values)==6):
			#Stand for 9
			if(digitToPattern[9]==""):
				if(digitToPattern[4]!=""):
					if(is_pattern_contained(digitToPattern[4], values)):
						digitToPattern[9] = values
			
			#Stand for 0
			elif(digitToPattern[0]==""):
				if(digitToPattern[9]!="" and digitToPattern[1]!="" and (not is_pattern_equal(values, digitToPattern[9]))):
					if(is_pattern_contained(digitToPattern[1], values)):
						digitToPattern[0] = values
			
			#Stand for 6
			elif(digitToPattern[6]==""):
				if(digitToPattern[9]!="" and digitToPattern[0]!=""):
					if((not is_pattern_equal(values, digitToPattern[9])) and (not is_pattern_equal(values, digitToPattern[0]))):
						digitToPattern[6] = values
		
		#Stand for 2 or 3 or 5
		if(len(values)==5):
			#Stand for 3
			if(digitToPattern[3]=="" and digitToPattern[7]!=""):
				if(is_pattern_contained(digitToPattern[7], values)):
					digitToPattern[3] = values
			#Stand for 5
			elif(digitToPattern[5]==""):
				if((digitToPattern[9]!="" or digitToPattern[6]!="") and (not is_pattern_equal(values, digitToPattern[3]))):
					if(is_pattern_contained(values, digitToPattern[6]) or is_pattern_contained(values, digitToPattern[9])):
						digitToPattern[5] = values
			
			#Stand for 2
			elif(digitToPattern[2]==""):
				if(digitToPattern[3]!="" and digitToPattern[5]!=""):
					if((not is_pattern_equal(values, digitToPattern[3])) and (not is_pattern_equal(values, digitToPattern[5]))):
						digitToPattern[2] = values
				


	
#1,4,7,8
def actualise_mapping_with_easy_patterns(patterns, output, digitToPattern):
	facility_easy_patterns(patterns,digitToPattern)
	facility_easy_patterns(output,digitToPattern)


#0,2,3,5,6,9
def actualise_mapping_with_harder_patterns(patterns, output,digitToPattern):
	facility_harder_patterns(patterns,digitToPattern)
	facility_harder_patterns(output,digitToPattern)

	
def determine_digitToPattern(patterns, output, digitToPattern):
	actualise_mapping_with_easy_patterns(patterns, output, digitToPattern)
	actualise_mapping_with_harder_patterns(patterns, output, digitToPattern)
	actualise_mapping_with_harder_patterns(patterns, output, digitToPattern)
	actualise_mapping_with_harder_patterns(patterns, output, digitToPattern)


	
	
def output_to_value(output, digitToPattern):
	finalNumber = ""
	for pattern in output:
		for i in range(len(digitToPattern)):
			patternFound = digitToPattern[i]
			if(is_pattern_equal(pattern, patternFound)):
				finalNumber+=str(i)
				break
				
	
	print(finalNumber)
	return int(finalNumber)
	

	
# part 1 : 25 min
# part 2 : 3h19 min
def exo8():
	print("#### EXO 8 #####")
	f = open('input_8.txt', 'r')
		
	lines = f.readlines()
	finalSum = 0
	
	for line in lines:
		digitToPattern = [""]*10
		
		patterns, output = line.split(' | ')
		patterns = patterns.split(' ')
		print("\npatterns: ", patterns)
	
		output = output.rstrip("\n")
		output = output.split(' ')
		print("output: ", output)
		
		determine_digitToPattern(patterns, output,digitToPattern)

		print("digitToPattern ", digitToPattern)
		
		finalSum += output_to_value(output, digitToPattern)
		
	
	print("\finalSum ", finalSum)
	
	
	f.close()




#1096964

exo8()
