
import re

def actualise_ventsDiagram_horizontal(x, yMin, yMax, ventsDiagram):
	for y in range(yMin, yMax+1):
		ventsDiagram[y][x] +=1

def actualise_ventsDiagram_vertical(y, xMin, xMax, ventsDiagram):
	for x in range(xMin, xMax+1):
		ventsDiagram[y][x] +=1


#(x1,yMin) (x2,yMax)
def actualise_ventsDiagram_diagonal(x1, x2, yMin, yMax, ventsDiagram):
	#DownLeft diagonal
	if (x1 > x2):
		iteration = x1 - x2
		for i in range (iteration+1):
			ventsDiagram[yMin+i][x1-i] +=1		
		
	#DownRight diagonal
	else :
		iteration = x2 - x1
		for i in range (iteration+1):
			ventsDiagram[yMin+i][x1+i] +=1
		


def actualise_ventsDiagram(firstCoordinate, secondCoordinate, ventsDiagram):
	x1,y1 = firstCoordinate
	x2,y2 = secondCoordinate
	
	if (x1 == x2):
		yMin = min(y1, y2)
		yMax = max(y1, y2)
		actualise_ventsDiagram_horizontal(x1, yMin, yMax, ventsDiagram)
	
	elif (y1 == y2):
		xMin = min(x1, x2)
		xMax = max(x1, x2)
		actualise_ventsDiagram_vertical(y1, xMin, xMax, ventsDiagram)
	
	else :
		if(y1 > y2):
			actualise_ventsDiagram_diagonal(x2, x1, y2, y1, ventsDiagram)
		else:
			actualise_ventsDiagram_diagonal(x1, x2, y1, y2, ventsDiagram)
	

def get_where_more_than_two_lines(ventsDiagram):
	y = 0
	filtered = []
	for line in (ventsDiagram):
		for x in range (len(line)):
			if (line[x] >= 2):
				filtered.append((x,y))
		y += 1
	
	return len(filtered), filtered

	
def print_ventsDiagram(ventsDiagram):
	for line in ventsDiagram:
		print(line)


#filtered = list(filter(lambda x: x >= 2.0, line))
#filtered = list(filter(lambda (x,y): x >= 2.0, line))

# 1h10 (+ 31 min for part 2 which is include in code)
def exo5_part1():
	print("#### EXO 5 part 1 #####")
	f = open('input_5.txt', 'r')
		

	#BEWARE here the size of diagram is hardcoded (aka = 1000)
	# but in theory should determine maxX and maxY to have the correct size
	ventsDiagram = [[0]*1000 for i in range(1000)]
	coordinatesList = f.readlines()
	
	for coordinates in coordinatesList:
		coordinates = coordinates.rstrip("\n")
		coordinates = re.split('-> |,', coordinates)
		#print(coordinates)
		
		intCoord1 = (int(coordinates[0]), int(coordinates[1]))
		intCoord2 = (int(coordinates[2]), int(coordinates[3]))
		#print(intCoord1, " ", intCoord2)
		actualise_ventsDiagram(intCoord1, intCoord2, ventsDiagram)
	
	#print_ventsDiagram(ventsDiagram)
	

	countAtLeastTwoLinesOverlap, listPointOverlap =get_where_more_than_two_lines(ventsDiagram)
	#print("countAtLeastTwoLinesOverlap ", countAtLeastTwoLinesOverlap, " listPointOverlap ", listPointOverlap)
	print("countAtLeastTwoLinesOverlap ", countAtLeastTwoLinesOverlap)





	f.close()


exo5_part1()
