# Python program for reading
# from file
 
 
# 19 min
def exo2_part1():
	print("#### EXO 2 part 1 #####")
	f = open('input_2.txt', 'r')
	
	finalHorizontal = 0
	finalDepth = 0
	
	lines = f.readlines()
	
	for line in lines:
		command, value = line.split(" ")
		#print(command, " ", value)
		
		if (command == "forward"):
			finalHorizontal += int(value)
		
		elif (command == "down"):
			finalDepth += int(value)
				
		elif (command == "up"):
			finalDepth -= int(value)
	
	f.close()
	
	print("Final Horizontal : ", finalHorizontal)
	print("Final Depth : ", finalDepth)
	print("Final result : ", finalDepth*finalHorizontal)


# 5 min
def exo2_part2():
	print("#### EXO 2 part 2 #####")
	f = open('input_2.txt', 'r')
	
	finalHorizontal = 0
	finalDepth = 0
	aim = 0
	
	lines = f.readlines()
	
	for line in lines:
		command, value = line.split(" ")
		#print(command, " ", value)
		
		if (command == "forward"):
			finalHorizontal += int(value)
			finalDepth += aim * int(value)
		
		elif (command == "down"):
			aim += int(value)
				
		elif (command == "up"):
			aim -= int(value)
	
	f.close()
	
	print("Final Horizontal : ", finalHorizontal)
	print("Final Depth : ", finalDepth)
	print("Final aim : ", aim)
	print("Final result : ", finalDepth*finalHorizontal)

exo2_part1()
exo2_part2()

