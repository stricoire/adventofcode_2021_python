import itertools




def print_data_points(dataPoints):
	for line in dataPoints:
		print(line)	



def count_visible_dots(dataPoints):
	count = 0
	for line in dataPoints:
		count += line.count('#')
	
	return count

def validity_function():
	assert(count_visible_dots(['#','#','#'])==3)
	assert(count_visible_dots([['#','#','#'],['#','#','#']])==6)
	assert(count_visible_dots([['#',')','#'],['u','#','#']])==4)
	assert(split_list_horizontal([[0,1,2,3],[4,5,6,7],[20,20,20,20],[8,9,10,11],[12,13,14,15]],2)==[[[0, 1, 2, 3], [4, 5, 6, 7]], [[8, 9, 10, 11], [12, 13, 14, 15]]])
	assert(split_list_vertical([[0,1,20,2,3],[4,5,20,6,7],[8,9,20,10,11],[12,13,20,14,15]],2)==[[[0, 1], [4, 5], [8, 9], [12, 13]], [[2, 3], [6, 7], [10, 11], [14, 15]]])
	assert(split_list_vertical([[0,20,1],[2,20,3],[4,20,5], [6,20,7]],1)==[[[0], [2], [4], [6]], [[1], [3], [5], [7]]])
	assert(merge_data_horizontal([['u','#','b'],['#','u','#']], [['#','u','#'],['u','#','b']])==[['#', '#', '#'], ['#', '#', '#']])
	assert(merge_data_horizontal([['u','#'],['#','u'],['u','u']], [['b','#'],['#','d'],['#','#']])==[['#','#'],['#','#'],['#','#']])
	assert(merge_data_vertical([['u','#'],['#','u'],['u','u']], [['#','b'],['d','#'],['#','#']])==[['#','#'],['#','#'],['#','u']])
	assert(merge_data_vertical([['u','#','u'],['#','u','#']], [['b','#','b'],['#','a','#']])==[['#','#','#'],['#','#','#']])

def merge_data_horizontal(data1, data2):
	merged_data = data1
	
	for y in range (len(data1)):
		for x in range (len(data1[0])):
			if (data2[y][len(data1[0])-1-x] == '#'):
				merged_data[y][x] = data2[y][len(data1[0])-1-x]
	
	#print(merged_data)
	return merged_data


def merge_data_vertical(data1, data2):
	merged_data = data1
	
	for y in range (len(data1)):
		for x in range (len(data1[0])):
			if (data2[len(data1) -1 - y][x] == '#'):
				merged_data[y][x] = data2[len(data1) -1 - y][x]
		
	return merged_data


def split_list_horizontal(dataPoints, splitY):
	chunks = [dataPoints[y:y+splitY] for y in range(0, len(dataPoints)+1, splitY+1)]
	return chunks



def split_list_vertical(dataPoints, splitX):
	chunks = [dataPoints[y][x:x+splitX] for x,y in itertools.product(range(0, len(dataPoints[0]), splitX+1),range(0, len(dataPoints)))]	
	splitY = int(len(chunks)/2)
	chunks = [chunks[y:y+splitY] for y in range(0, len(chunks), splitY)]

	return chunks	
	
# part 1 : 2h27
# part 2 : 2 min
def exo13():
	
	print("#### EXO 13 #####")
	f = open('input_13.txt', 'r')
	
	validity_function()
	
	lines = f.readlines()
	b_foldInstructions = False
	
	
	maxX = 0
	maxY = 0
	for line in lines :
		line = line.rstrip("\n")
		if (line == ""):
			break
		
		elif (not b_foldInstructions):
			line = line.split(',')
			if(int(line[0]) > maxX):
				maxX = int(line[0])
			if(int(line[1]) > maxY):
				maxY = int(line[1])

	maxX += 1
	maxY += 1


	dataPoints = [['.']*maxX for i in range(maxY)]
	
	for line in lines:
		line = line.rstrip("\n")
		if (line == ""):
			b_foldInstructions = True
		
		elif (not b_foldInstructions):
			line = line.split(',')
			dataPoints[int(line[1])][int(line[0])] = '#'
		
		elif (b_foldInstructions):
			line = line.split()
			line = line[2]
			line = line.split('=')
			
			if(line[0] == 'x'):
				dataPoints = split_list_vertical(dataPoints, int(line[1]))
				dataPoints = merge_data_horizontal(dataPoints[0],dataPoints[1])
				#print_data_points(dataPoints)
				print("Count ",count_visible_dots(dataPoints))
			
			elif(line[0] == 'y'):
				dataPoints = split_list_horizontal(dataPoints, int(line[1]))
				dataPoints = merge_data_vertical(dataPoints[0],dataPoints[1])
				#print_data_points(dataPoints)
				print("Count ",count_visible_dots(dataPoints))
				
				
			
		
	
	print_data_points(dataPoints)

	
	f.close()


exo13()


