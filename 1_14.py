import collections



def actualise_pair_count(pairInsertion, pairCount, nextPairCount, step):
	for i in range (step):
		for pair in pairInsertion:
			if (pairCount[pair] > 0):
				newElement = pairInsertion[pair]
				newPair1 = pair[0] + newElement
				newPair2 = newElement + pair[1]
				nextPairCount[newPair1] += pairCount[pair]
				nextPairCount[newPair2] += pairCount[pair]
				pairCount[pair] = 0
				nextPairCount[pair] -= pairCount[pair]
		
		for pair in pairInsertion:
			pairCount[pair] = nextPairCount[pair]
			nextPairCount[pair] = 0
	
	return pairCount
			

def count_each_element(pairCount, polymerTemplate):
	countElement = {}
	for pair in pairCount:
		for element in pair:
			if (pairCount[pair] > 0):
				if (element not in countElement):
					countElement[element] = pairCount[pair]
				else:
					countElement[element] += pairCount[pair]
	print(countElement)
	for element in countElement:
		countElement[element] = int(countElement[element]/2)
	
	countElement[polymerTemplate[0]] += 1
	countElement[polymerTemplate[-1]] += 1
	return countElement
	
def get_final_result(countElement):
	print(countElement)
	mostCommonElement = max(countElement, key=countElement.get)
	leastCommonElement = min(countElement, key=countElement.get)
	
	print(mostCommonElement," ", countElement[mostCommonElement])
	print(leastCommonElement," ", countElement[leastCommonElement])
	
	finalResult = countElement[mostCommonElement] - countElement[leastCommonElement]
	return finalResult


# part 1 : 30
# part 2 : 2h
def exo14():
	
	print("#### EXO 14 #####")
	f = open('input_14.txt', 'r')
	

	
	polymerTemplate = f.readline().rstrip("\n")

	# Skip the blank line
	f.readline()
	
	# Parse pair insertion
	lines = f.readlines()
	pairInsertion = {}
	pairCount = {}
	nextPairCount = {}
	for line in lines:
		line = line.rstrip("\n")
		line = line.split(" -> ")
		pairInsertion[line[0]] = line[1]
		pairCount[line[0]] = 0
		nextPairCount[line[0]] = 0
	
	
	for element1, element2 in zip(polymerTemplate, polymerTemplate[1:]):
		pairCount[element1+element2] += 1
	
	pairCount = actualise_pair_count(pairInsertion, pairCount, nextPairCount, 40)
	
	
	countElement = count_each_element(pairCount,polymerTemplate)
	print(get_final_result(countElement))
	
	f.close()


exo14()


