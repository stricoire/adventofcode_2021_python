



gVar_FlashCount = 0


def print_data_points(dataPoints):
	for line in dataPoints:
		print(line)	

def validity_function():
	#assert(is_chunk_closed(')'))
	a= 5
	
	assert(increase_energy_level([[0],[1],[2]])==[[1],[2],[3]])
	assert(increase_energy_level([[-1,-1],[7,8],[104,-2005]])==[[0,0],[8,9],[105,-2004]])
	assert(increase_energy_level_neighbor([[0],[1],[2]], 0, 0)==[[0],[2],[2]])
	assert(increase_energy_level_neighbor([[9,9,9],[9,0,9],[9,9,9]], 1, 1)==[[0,0,0],[0,0,0],[0,0,0]])
	assert(increase_energy_level_neighbor([[2,2,2,2,2],[2,0,10,10,2],[2,10,2,10,2], [2,10,10,10,2],[2,2,2,2,2]], 1, 1)==[[3,4,5,4,3],[4,0,0,0,4],[5,0,0,0,5], [4,0,0,0,4],[3,4,5,4,3]])
	assert(is_synch_flashing([[0],[0]]))
	assert(is_synch_flashing([[0],[1]]))==False


def increase_energy_level(octopusLevelEnergyList):
	return [[z+1 for z in y] for y in octopusLevelEnergyList]


def increase_energy_level_neighbor(octopusLevelEnergyList, xCurrent, yCurrent):
	global gVar_FlashCount
	
	maxY = min(len(octopusLevelEnergyList), yCurrent + 2)
	maxX = min(len(octopusLevelEnergyList[0]), xCurrent + 2)
	
	minY = max(0, yCurrent-1)
	minX = max(0, xCurrent-1)

	
	for y in range(minY, maxY):
		for x in range(minX, maxX):
			if (x == xCurrent) or (y==yCurrent):
				pass
			if (octopusLevelEnergyList[y][x] != 0):
				octopusLevelEnergyList[y][x] += 1
			
			
			if (octopusLevelEnergyList[y][x] > 9):
				octopusLevelEnergyList[y][x] = 0
				gVar_FlashCount += 1
				increase_energy_level_neighbor(octopusLevelEnergyList, x, y)
	return octopusLevelEnergyList
	

def is_synch_flashing(octopusLevelEnergyList):
	return all([all([z==0 for z in x]) for x in octopusLevelEnergyList])
	

# part 1 : 1h17
# part 2 : 24 min
def exo11():
	global gVar_FlashCount
	
	print("#### EXO 11 #####")
	f = open('input_11.txt', 'r')
		
	lines = f.readlines()
	
	validity_function()
	
	octopusLevelEnergyList = []
	
	#Parsing data
	for line in lines:
		line = line.rstrip("\n")
		data = []
		for octopusLevelEnergy in line:
			data.append(int(octopusLevelEnergy))
		
		octopusLevelEnergyList.append(data)	
	
	stepCount = 400
	gVar_FlashCount = 0
	b_synchFlash = False
	
	for step in range (stepCount):
		octopusLevelEnergyList = increase_energy_level(octopusLevelEnergyList)
		for y in range (len(octopusLevelEnergyList)):
			for x in range (len(octopusLevelEnergyList[0])):
				if (octopusLevelEnergyList[y][x] > 9):
					octopusLevelEnergyList[y][x] = 0
					gVar_FlashCount += 1
					increase_energy_level_neighbor(octopusLevelEnergyList, x, y)
					
					if(is_synch_flashing(octopusLevelEnergyList)):
						b_synchFlash = True
						break
			
			if (b_synchFlash):
				break
		
		if (b_synchFlash):
			print("CurrentStep synchro flash : ",step+1)
			break 
	
	
		print_data_points(octopusLevelEnergyList)
		print("\n")
	
	is_synch_flashing(octopusLevelEnergyList)
	print(gVar_FlashCount)
	
	f.close()


exo11()


