

def create_lanternfish_population_referral(lanternFishData, maxDaysBeforeNewLanternfish):
	lanternfishPopulation = [0]*(maxDaysBeforeNewLanternfish+1)
	
	for fish in lanternFishData:
		lanternfishPopulation[int(fish)] +=1
	
	return lanternfishPopulation


def update_population(lanternfishPopulation, maxDaysBeforeNewLanternfish):
	dataCopy = lanternfishPopulation.copy()
	
	normDayBeforeNewLantern = 6
	
	
	for currentDay in range (len(lanternfishPopulation)-1,-1,-1):	
		if (currentDay == 0):
			lanternfishPopulation[maxDaysBeforeNewLanternfish] = dataCopy[0]
			lanternfishPopulation[normDayBeforeNewLantern] += dataCopy[0]
			lanternfishPopulation[currentDay] = dataCopy[currentDay+1]
		
		elif (currentDay != maxDaysBeforeNewLanternfish):
			lanternfishPopulation[currentDay] = dataCopy[currentDay+1]
		
	return lanternfishPopulation



# 46 min part 1 / 1 min part 2
def exo6():
	print("#### EXO 6 #####")
	f = open('input_6.txt', 'r')
		
	lines = f.readline()
	lanternFishData = lines.split(',')
	
	print(lanternFishData)

	lanternfishPopulation = create_lanternfish_population_referral(lanternFishData, 8)
	print (lanternfishPopulation)
	
	daysCount = 256
	for i in range (daysCount):
		lanternfishPopulationReferral = update_population(lanternfishPopulation,8)
	print (lanternfishPopulation)
	
	print("Total population is ", sum(lanternfishPopulation))

	f.close()


exo6()
